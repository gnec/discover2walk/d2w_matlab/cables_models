

clear all 
close all

figure;

%MOTORES
K_0 = [0, 0, 0]';
a1 = K_0 + [ 0  1   0.5 ]';
a2 = K_0 + [ 0  1  -0.5 ]';
a3 = K_0 + [ 0 -1  -0.5 ]';
a4 = K_0 + [ 0 -1   0.5 ]';
%MOT AUX
a1_1 = K_0 + [  0.7  1   0.5]';
a1_2 = K_0 + [ -0.7  1   0.5]';
a2_1 = K_0 + [  0.7  1  -0.5]'; 
a2_2 = K_0 + [ -0.7  1  -0.5]';
a3_1 = K_0 + [  0.7 -1  -0.5]';
a3_2 = K_0 + [ -0.7 -1  -0.5]';
a4_1 = K_0 + [  0.7 -1   0.5]';
a4_2 = K_0 + [ -0.7 -1   0.5]';

%PIE
b1_0=[ -0.4  0.7 0]';
b2_0=[ -0.4 -0.7 0]';
b3_0=[  0.4 -0.7 0]';
b4_0=[  0.4  0.7 0]';


%%

%Cinematica
roll  = 0;
pitch = 0;
yaw   = 0;
R = FUNCS.generalRotationMatrix(roll,pitch,yaw);
r = [0,0,0]';
 
l1 = FUNCS.inverseCableKinematics(a1_1,r,R,b4_0);
l2 = FUNCS.inverseCableKinematics(a1_2,r,R,b1_0);
l3 = FUNCS.inverseCableKinematics(a2_1,r,R,b4_0);
l4 = FUNCS.inverseCableKinematics(a2_2,r,R,b1_0);
l5 = FUNCS.inverseCableKinematics(a3_1,r,R,b3_0);
l6 = FUNCS.inverseCableKinematics(a3_2,r,R,b2_0);
l7 = FUNCS.inverseCableKinematics(a4_1,r,R,b3_0);
l8 = FUNCS.inverseCableKinematics(a4_2,r,R,b2_0);

%New B points
b1 = -(l2 - a1_2)';
b2 = -(l8 - a4_2)';
b3 = -(l7 - a4_1)';
b4 = -(l1 - a1_1)';

%Lenths of the cables
[desirable_lengths,desirable_norm_lengths] = FUNCS.save_lenths(l1,l2,l3,l4,l5,l6,l7,l8);

%Plot of the robot
FUNCS.graphicsPlot(a1,a2,a3,a4,...
                  b1,b2,b3,b4,...
                  a1_1,a1_2,a2_1,a2_2,a3_1,a3_2,a4_1,a4_2)