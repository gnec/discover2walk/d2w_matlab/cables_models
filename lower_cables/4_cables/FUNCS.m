classdef FUNCS
    methods(Static)
        
        % General cable inverse kinematics
        function l = inverseCableKinematics (a,r,R,b)
            l = a - r - (R * b);
        end
        
        function R = generalRotationMatrix(roll,pitch,yaw)
            deg2rad = 2*3.141516/360;
            roll  = roll *deg2rad;
            pitch = pitch *deg2rad;
            yaw   = yaw *deg2rad;
            % Axis rotation matrixes
            R_x=[    1              0               0        ;    0         cos(roll)       -sin(roll) ;      0             sin(roll)       cos(roll) ];
            R_y=[cos(pitch)         0           sin(pitch)   ;    0            1                0      ; -sin(pitch)            0           cos(pitch)];
            R_z=[ cos(yaw)      -sin(yaw)           0        ; sin(yaw)     cos(yaw)            0      ;      0                 0                1    ];
            % General matrixes
            R = R_z * R_y * R_x;   
        end

        function plotVector(p , v)
            plot3([p(1) v(1)], [p(2) v(2)], [p(3) v(3)],'Color', [72/255 206/255 227/255 0.3],'LineWidth',3);
        end

        function graphicsPlot(a1,a2,a3,a4,...
                              b1,b2,b3,b4,...
                              a1_1,a1_2,a2_1,a2_2,a3_1,a3_2,a4_1,a4_2)

            hold on;
            
            %Motores
            text(a1(1),a1(2),a1(3),"A1",'Color','green','FontSize',14)
            text(a2(1),a2(2),a2(3),"A2",'Color','green','FontSize',14)
            text(a3(1),a3(2),a3(3),"A3",'Color','green','FontSize',14)
            text(a4(1),a4(2),a4(3),"A4",'Color','green','FontSize',14)
            %Tambores A1
            text(a1_1(1),a1_1(2),a1_1(3),"A1_1",'Color','blue','FontSize',14)
            text(a1_2(1),a1_2(2),a1_2(3),"A1_2",'Color','blue','FontSize',14)
            FUNCS.plotVector(a1_1,a1_2);
            %Tambores A2
            text(a2_1(1),a2_1(2),a2_1(3),"A2_1",'Color','blue','FontSize',14)
            text(a2_2(1),a2_2(2),a2_2(3),"A2_2",'Color','blue','FontSize',14)
            FUNCS.plotVector(a2_1,a2_2);
            %Tambores A3
            text(a3_1(1),a3_1(2),a3_1(3),"A3_1",'Color','blue','FontSize',14)
            text(a3_2(1),a3_2(2),a3_2(3),"A3_2",'Color','blue','FontSize',14)
            FUNCS.plotVector(a3_1,a3_2);
            %Tambores A4
            text(a4_1(1),a4_1(2),a4_1(3),"A4_1",'Color','blue','FontSize',14)
            text(a4_2(1),a4_2(2),a4_2(3),"A4_2",'Color','blue','FontSize',14)
            FUNCS.plotVector(a4_1,a4_2);
            
            %Pie
            plot3([b1(1) b2(1)], [b1(2) b2(2)], [b1(3) b2(3)],'Color', [72/255 206/255 227/255 0.3],'LineWidth',3);
            plot3([b2(1) b3(1)], [b2(2) b3(2)], [b2(3) b3(3)],'Color', [72/255 206/255 227/255 0.3],'LineWidth',3);
            plot3([b3(1) b4(1)], [b3(2) b4(2)], [b3(3) b4(3)],'Color', [72/255 206/255 227/255 0.3],'LineWidth',3);
            plot3([b4(1) b1(1)], [b4(2) b1(2)], [b4(3) b1(3)],'Color', [72/255 206/255 227/255 0.3],'LineWidth',3);
            X = [b1(1) b2(1) b3(1) b4(1)];
            Y = [b1(2) b2(2) b3(2) b4(2)];
            Z = [b1(3) b2(3) b3(3) b4(3)];
            fill3(X,Y,Z,'g');
            grid on;
            text(b1(1),b1(2),b1(3),"B1",'Color','green','FontSize',14)
            text(b2(1),b2(2),b2(3),"B2",'Color','green','FontSize',14)
            text(b3(1),b3(2),b3(3),"B3",'Color','green','FontSize',14)
            text(b4(1),b4(2),b4(3),"B4",'Color','green','FontSize',14)
            view([1 1 1]); 
            xlim([-1 1])
            
            %Cables
            FUNCS.plotVector(b1, a1_2);
            FUNCS.plotVector(b1, a2_2);
            FUNCS.plotVector(b2, a4_2);
            FUNCS.plotVector(b2, a3_2);
            FUNCS.plotVector(b3, a4_1);
            FUNCS.plotVector(b3, a3_1);
            FUNCS.plotVector(b4, a1_1);
            FUNCS.plotVector(b4, a2_1);

        
        end
        function [desirable_lengths,desirable_norm_lengths] = save_lenths(l1,l2,l3,l4,l5,l6,l7,l8)
            % Cables lengths
            l1_mod = norm(l1);
            l2_mod = norm(l2);
            l3_mod = norm(l3);
            l4_mod = norm(l4);
            l5_mod = norm(l5);
            l6_mod = norm(l6);
            l7_mod = norm(l7);
            l8_mod = norm(l8);
            
            %Guarda valores
            desirable_lengths = [l1,l2,l3,l4,l5,l6,l7,l8]';
            desirable_norm_lengths = [l1_mod,l2_mod,l3_mod,l4_mod,l5_mod,l6_mod,l7_mod,l8_mod]';
        end

    end
end

