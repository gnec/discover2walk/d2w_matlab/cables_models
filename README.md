# Discover2Walk Cables Models
In this repository you will find MATLAB scripts of several Discover2Walk (D2W) robotic sistem cables configurations.

The main modules are:
1. **lower_cables**: Cable-driven robotic module in charge of controlling the ankles/feet positions and orientations during gait or foot rehabilitation.
2. **pelvis_cables**:  Cable-driven robotic module in charge of controlling the pelvis orientation, position and users body weight support.


## Open repository in MATLAB as Git Project 
To open this repository in MATLAB 2022b and keep track of the changes you need to create a matlab project folliwing these instructions:

1. Open MATLAB, in "home" select "New"->"Project"->"From Git". This action will open a menu with several options.
2. Select "Project from Git".
3. In the "Repository path" select the https direction of this repository "https://gitlab.com/gnec/discover2walk/d2w_matlab/cables_models.git" and clic in "Retrieve". It will ask you to create a specified sandbox folder, create it. 
4. Select the name of the local project: "cables_models" and continue with the set up, you do not need to create or add any more files, just click next and finish. 
5. At this point you have access to the simulink scripts and any git add/comit/fetch/push/pull... command to keep track of your changes. 

## Branches
There are 3 subbranches in case you want to work with the latest:
- *pelvis*: 4 cables solution to control the pelvis.
- *ankles*: 4 cables solution to control the ankles positions x,y,z control.
- *lower_4_cables*: 4 cables solution for to controll the x,y and pitch of the feet.
- *lower_8_cables*: 8 cables solution for the 6 DOF control of the feet.
