classdef FUNCS
    methods(Static)
        
        function r_solv =  fordwardPelvisKinematics(l_cables,R,a1,a2,a3,a4,b1,b2,b3,b4)
        
            % For a given pelvis rotation (R) and cables length (l) compute pelvis 
            % center position (r) respect robot reference system.
            % It is calculated by minimizing the error between the cables modules
            % and the desired cables lengths using de vector "r = [r_x, r_y, r_z]"
            % as the optimization variable.
            %
            %      Ψ_i(l,r,R) = module(a_i - r - R*b_i)^2 - l_i^2 = 0
            %
            %      Φ(l) = min Σ(Ψ_i)   
            %
            % The optimization is performed with lsqnonlin: 
            % nonlinear least squares (nonlinear data fitting)
            l1 = l_cables(1);
            l2 = l_cables(2);
            l3 = l_cables(3);
            l4 = l_cables(4);
            prob = optimproblem('ObjectiveSense','min');
            r = optimvar('r',3,1,'LowerBound',[-1 -1 0],'UpperBound',[1 1 1.2]);
            prob.Objective = (norm(a1 - r - R*b1)^2 - l1^2)^2 + ...
                             (norm(a2 - r - R*b2)^2 - l2^2)^2 + ...
                             (norm(a3 - r - R*b3)^2 - l3^2)^2 + ...
                             (norm(a4 - r - R*b4)^2 - l4^2)^2;
            x0.r = [0 1 0];
              
            solv = solve(prob,x0);
            r_solv = solv.r;
        end
        
        % Inverse kinematics function
        function [l1,l2,l3,l4] = inversePelvisKinematics(r,R,a1,a2,a3,a4,b1,b2,b3,b4)
            % Compute Cables vectores
            l1 = FUNCS.inverseCableKinematics(a1,r,R,b1);
            l2 = FUNCS.inverseCableKinematics(a2,r,R,b2);
            l3 = FUNCS.inverseCableKinematics(a3,r,R,b3);
            l4 = FUNCS.inverseCableKinematics(a4,r,R,b4);
            
            % Cables lengths
            l1_mod = norm(l1);
            l2_mod = norm(l2);
            l3_mod = norm(l3);
            l4_mod = norm(l4);
            
            % B points in the world reference system:
            b1_O = -(l1 - a1)';
            b2_O = -(l2 - a2)';
            b3_O = -(l3 - a3)';
            b4_O = -(l4 - a4)';   
         
            % PLOT CABLES
            hold on
            arrow(b1_O, a1,'EdgeColor',[0.502, 0.251,0],'FaceColor',[0.502, 0.251,0],'width',1,'length',0);
            arrow(b2_O, a2,'EdgeColor',[0.502, 0.251,0],'FaceColor',[0.502, 0.251,0],'width',1,'length',0);
            arrow(b3_O, a3,'EdgeColor',[0.502, 0.251,0],'FaceColor',[0.502, 0.251,0],'width',1,'length',0);
            arrow(b4_O, a4,'EdgeColor',[0.502, 0.251,0],'FaceColor',[0.502, 0.251,0],'width',1,'length',0);
            text(a1(1),a1(2),a1(3),"A1",'Color','green','FontSize',14)
            text(a2(1),a3(2),a2(3),"A2",'Color','green','FontSize',14)
            text(a3(1),a2(2),a3(3),"A3",'Color','green','FontSize',14)
            text(a4(1),a4(2),a4(3),"A4",'Color','green','FontSize',14)
            text(b1_O(1),b1_O(2),b1_O(3),"B1",'Color','green','FontSize',14)
            text(b2_O(1),b2_O(2),b2_O(3),"B2",'Color','green','FontSize',14)
            text(b3_O(1),b3_O(2),b3_O(3),"B3",'Color','green','FontSize',14)
            text(b4_O(1),b4_O(2),b4_O(3),"B4",'Color','green','FontSize',14)
            % Plot Platform (pelvis)
            P=[b1_O ; b2_O ; b3_O ; b4_O];
            X=P(:,1);Y=P(:,2);Z=P(:,3);
            patch('XData',X,'YData',Y,'ZData',Z,'EdgeColor','green','FaceColor',[.2 .8 .2],'LineWidth',2)
            grid on;
            view([1 1 1]);
        end
        
        % Cables forces computation
        function [F_r_C] = forcesComputation(l1,l2,l3,l4,F_n,M_n,r,a1,a2,a3,a4,b1,b2,b3,b4)
            
            % FORCES COMPUTATION:
            %
            % By estabishing some geometric relationships it is possible to obtain 
            % the corresponding cable tensions (F_r) for a given resultant force 
            % vector (F_n_c) and a torque vector (M_n_C) applied at the center of 
            % the pelvis:
            %
            % F_r:   Force vector of the cables measured tensions: 
            %              [|F1|, |F2|, |F3|, |F4|]'   R^4 
            %
            % Fi:    Cartesian force vector of the cables tensions:
            %              [Fi(x), Fi(y), Fi(z)]'      R^3
            %
            % F_n_C: Cartesin force vector of the resultant force due to 
            %        the cables tensions:
            %              [F_n(x), F_n(y), F_n(z)]'   R^3
            %
            % M_n_C: Cartesian torque vector of the resultant torque due to
            %        the cables tensions:
            %              [M_n(x), M_n(y), M_n(z)]'   R^3
            %
            % W_n_c: Cartesian force and torque vectors due to the cables 
            %        tensions:
            %               [F_n' , M_n']'             R^6
            %
            % T_f:   Transformation matrix from F_r to F_n_C  (R^4 -> R^3)
            %
            % T_m:   Transformation matrix from F_r to M_n_C  (R^4 -> R^3)
            %
            % T:     Transformation matrix from F_r to W_n_c  (R^4 -> R^6)
        
            % Cables lengths
            l1_mod = norm(l1);
            l2_mod = norm(l2);
            l3_mod = norm(l3);
            l4_mod = norm(l4);
             
            % Compute the cables orientation respect the robot frame
            % cable_orientation_i = [alpha,betha,gamma]
            % alpha - > x angle
            % betha - > y angle
            % gamma - > z angle    
            % Orientation of cable 1
            cable_orientation_1 = [acos(l1(1)/l1_mod),  acos(l1(2)/l1_mod), acos(l1(3)/l1_mod)]; 
            % Orientation of cable 2
            cable_orientation_2 = [acos(l2(1)/l2_mod),  acos(l2(2)/l2_mod),  acos(l2(3)/l2_mod)]; 
            % Orientation of cable 3
            cable_orientation_3 = [acos(l3(1)/l3_mod),  acos(l3(2)/l3_mod),  acos(l3(3)/l3_mod)]; 
            % Orientation of cable 4
            cable_orientation_4 = [acos(l4(1)/l4_mod),  acos(l4(2)/l4_mod),  acos(l4(3)/l4_mod)];    
            
        
            % From the cables orientation information compute the transformation
            % matrices:
            %
            % T_f: Transformation matrix from F_r to F_n
            T_f = [cos(cable_orientation_1(1)) , cos(cable_orientation_2(1)) , cos(cable_orientation_3(1)) , cos(cable_orientation_4(1));
                   cos(cable_orientation_1(2)) , cos(cable_orientation_2(2)) , cos(cable_orientation_3(2)) , cos(cable_orientation_4(2));
                   cos(cable_orientation_1(3)) , cos(cable_orientation_2(3)) , cos(cable_orientation_3(3)) , cos(cable_orientation_4(3))];
        
            % T_f simplified:      
            T_f = [ l1(1)/l1_mod , l2(1)/l2_mod , l3(1)/l3_mod , l4(1)/l4_mod;
                    l1(2)/l1_mod , l2(2)/l2_mod , l3(2)/l3_mod , l4(2)/l4_mod;
                    l1(3)/l1_mod , l2(3)/l2_mod , l3(3)/l3_mod , l4(3)/l4_mod;];
        
            % T_m: Transformation matrix F_r to M_n
            T_m = [ b1(2)*l1(3)/l1_mod-b1(3)*l1(2)/l1_mod    ,    b2(2)*l2(3)/l2_mod-b2(3)*l2(2)/l2_mod    ,    b3(2)*l3(3)/l3_mod-b3(3)*l3(2)/l3_mod    ,    b4(2)*l4(3)/l4_mod-b4(3)*l4(2)/l4_mod ;
                    b1(3)*l1(1)/l1_mod-b1(1)*l1(3)/l1_mod    ,    b2(3)*l2(1)/l2_mod-b2(1)*l2(3)/l2_mod    ,    b3(3)*l3(1)/l3_mod-b3(1)*l3(3)/l3_mod    ,    b4(3)*l4(1)/l4_mod-b4(1)*l4(3)/l4_mod ;
                    b1(1)*l1(2)/l1_mod-b1(2)*l1(1)/l1_mod    ,    b2(1)*l2(2)/l2_mod-b2(2)*l2(1)/l2_mod    ,    b3(1)*l3(2)/l3_mod-b3(2)*l3(1)/l3_mod    ,    b4(1)*l4(2)/l4_mod-b4(2)*l4(1)/l4_mod ];
            
            % T: Transformation matrix F_r to W_n
            T   = [T_f ; T_m];
        
            % Once obtained the transformation matrices generate, obtain the
            % corresponding F_r vector for the desired F_n and M_n:
            W_n = [F_n' , M_n']';
            F_r = [0,0,0,0];
        
            % Solve the linear system equations and if the system is 
            % underdetermined (more unknowns than equations) or 
            % overdetermined(more equations than unknowns), mldivide 
            % will give the least squares solution to be as similar as 
            % possible to the desired F_n and M_n parameters:
            F_r = mldivide(T,W_n);
        
            % Recompute the new F_n and M_n vectors:
            F_n = T_f * F_r;    
            M_n = T_m * F_r;
        
        
            % Get cartesian values of measured cable forces: 
            % Cable force vector: Fi= [Fi_x, Fi_y, Fi_z]
            % Matrix of cable forces: F_r_C = [F1;F2;F3;F4]
            % Obtained from the elementwise multiplication fo the vector 
            % F_r and T_f
            F_r_C = F_r .* T_f';
        
            
            % PLOT FORCES
            %
            % Pelvis attachements positions respect the robot reference system:
            b1_O = -(l1 - a1)';
            b2_O = -(l2 - a2)';
            b3_O = -(l3 - a3)';
            b4_O = -(l4 - a4)';
            % Plot force plot scales
            force_scale = 0.002;
            momentum_scale = 0.0004;    
            hold on
            % F_1 -> F_r_C(1)_x, F_r_C(1)_y, F_r_C(1)_z
            quiver3(b1_O(1),b1_O(2),b1_O(3),F_r_C(1,1),F_r_C(1,2),F_r_C(1,3),force_scale,'k','filled','LineWidth',2);
            quiver3(b1_O(1),b1_O(2),b1_O(3),F_r_C(1,1),b1_O(2),b1_O(3),force_scale,'r','filled','LineWidth',1.5);
            quiver3(b1_O(1),b1_O(2),b1_O(3),b1_O(1),F_r_C(1,2),b1_O(3),force_scale,'g','filled','LineWidth',1.5);
            quiver3(b1_O(1),b1_O(2),b1_O(3),b1_O(1),b1_O(2),F_r_C(1,3),force_scale,'b','filled','LineWidth',1.5);  
            % F_2 -> F_r_C(2)_x, F_r_C(2)_y, F_r_C(2)_z
            quiver3(b2_O(1),b2_O(2),b2_O(3),F_r_C(2,1),F_r_C(2,2),F_r_C(2,3),force_scale,'k','filled','LineWidth',2);
            quiver3(b2_O(1),b2_O(2),b2_O(3),F_r_C(2,1),b2_O(2),b2_O(3),force_scale,'r','filled','LineWidth',1.5);
            quiver3(b2_O(1),b2_O(2),b2_O(3),b2_O(1),F_r_C(2,2),b2_O(3),force_scale,'g','filled','LineWidth',1.5);
            quiver3(b2_O(1),b2_O(2),b2_O(3),b2_O(1),b2_O(2),F_r_C(2,3),force_scale,'b','filled','LineWidth',1.5);    
            % F_3 -> F_r_C(3)_x, F_r_C(3)_y, F_r_C(3)_z
            quiver3(b3_O(1),b3_O(2),b3_O(3),F_r_C(3,1),F_r_C(3,2),F_r_C(3,3),force_scale,'k','filled','LineWidth',2);
            quiver3(b3_O(1),b3_O(2),b3_O(3),F_r_C(3,1),b3_O(2),b3_O(3),force_scale,'r','filled','LineWidth',1.5);
            quiver3(b3_O(1),b3_O(2),b3_O(3),b3_O(1),F_r_C(3,2),b3_O(3),force_scale,'g','filled','LineWidth',1.5);
            quiver3(b3_O(1),b3_O(2),b3_O(3),b3_O(1),b3_O(2),F_r_C(3,3),force_scale,'b','filled','LineWidth',1.5);    
            % F_4 -> F_r_C(4)_x, F_r_C(4)_y, F_r_C(4)_z
            quiver3(b4_O(1),b4_O(2),b4_O(3),F_r_C(4,1),F_r_C(4,2),F_r_C(4,3),force_scale,'k','filled','LineWidth',2);
            quiver3(b4_O(1),b4_O(2),b4_O(3),F_r_C(4,1),b4_O(2),b4_O(3),force_scale,'r','filled','LineWidth',1.5);
            quiver3(b4_O(1),b4_O(2),b4_O(3),b4_O(1),F_r_C(4,2),b4_O(3),force_scale,'g','filled','LineWidth',1.5);
            quiver3(b4_O(1),b4_O(2),b4_O(3),b4_O(1),b4_O(2),F_r_C(4,3),force_scale,'b','filled','LineWidth',1.5);  
            % F_n -> F_n_x, F_n_y, F_n_z
            quiver3(r(1),r(2),r(3),F_n(1),F_n(2),F_n(3),force_scale,'k','filled','LineWidth',2);
            quiver3(r(1),r(2),r(3),F_n(1),r(2),r(3),force_scale,'r','filled','LineWidth',1.5);
            quiver3(r(1),r(2),r(3),r(1),F_n(2),r(3),force_scale,'g','filled','LineWidth',1.5);
            quiver3(r(1),r(2),r(3),r(1),r(2),F_n(3),force_scale,'b','filled','LineWidth',1.5);    
            % M_n -> M_n_x, M_n_y, M_n_z
            quiver3(r(1),r(2),r(3),M_n(1),M_n(2),M_n(3),momentum_scale,'c','filled','LineWidth',3);
            text(b1_O(1) + F_r_C(1,1)*force_scale,b1_O(2) + F_r_C(1,2)*force_scale,b1_O(3) + F_r_C(1,3)*force_scale,"F1",'Color','black','FontSize',14)
            text(b2_O(1) + F_r_C(2,1)*force_scale,b2_O(2) + F_r_C(2,2)*force_scale,b2_O(3) + F_r_C(2,3)*force_scale,"F2",'Color','black','FontSize',14)
            text(b3_O(1) + F_r_C(3,1)*force_scale,b3_O(2) + F_r_C(3,2)*force_scale,b3_O(3) + F_r_C(3,3)*force_scale,"F3",'Color','black','FontSize',14)
            text(b4_O(1) + F_r_C(4,1)*force_scale,b4_O(2) + F_r_C(4,2)*force_scale,b4_O(3) + F_r_C(4,3)*force_scale,"F4",'Color','black','FontSize',14)
            text(r(1) + F_n(1)*force_scale, r(2) + F_n(2)*force_scale, r(3) + F_n(3)*force_scale,"F_n",'Color','black','FontSize',18)
            text(r(1) + M_n(1)*momentum_scale, r(2) + M_n(2)*momentum_scale, r(3) + M_n(3)*momentum_scale,"M_n",'Color','black','FontSize',18)
            grid on;
            view([1 1 1]);
        
        end
        
        % General cable inverse kinematics
        function l = inverseCableKinematics (a,r,R,b)
            l = a - r - (R * b);
        end
        
        function R = generalRotationMatrix(roll,pitch,yaw)
            deg2rad = 2*3.141516/360;
            roll  = roll *deg2rad;
            pitch = pitch *deg2rad;
            yaw   = yaw *deg2rad;
            % Axis rotation matrixes
            R_x=[    1              0               0        ;    0         cos(roll)       -sin(roll) ;      0             sin(roll)       cos(roll) ];
            R_y=[cos(pitch)         0           sin(pitch)   ;    0            1                0      ; -sin(pitch)            0           cos(pitch)];
            R_z=[ cos(yaw)      -sin(yaw)           0        ; sin(yaw)     cos(yaw)            0      ;      0                 0                1    ];
            % General matrixes
            R = R_z * R_y * R_x;   
        end

    end
end

