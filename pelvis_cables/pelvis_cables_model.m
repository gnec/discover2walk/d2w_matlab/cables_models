clear all 
close all

%% User parameters
% Antropometric user variables
% Winter Tables:
patient_height = 1.7;
leg_length    = 0.530 * patient_height;
pelvis_width  = 0.191 * patient_height;
thigh_length  = 0.246 * patient_height;
shank_length  = 0.246 * patient_height;
foot_length   = 0.152 * patient_height;
foot_height   = 0.039 * patient_height;
leg_dimentions     = [  0           0          -0.530 ]' * patient_height; % m
pelvis_dimentions  = [  0           0.191 /2   -0.100 ]' * patient_height; % m
shank_dimentions   = [  0           0          -0.246 ]' * patient_height; % m
thigh_dimentions   = [  0           0          -0.246 ]' * patient_height; % m
foot_dimentions    = [ -0.122/1.5   0          -0.039 ]' * patient_height; % m
heel_position      = [  0.03/1.5    0          -0.039 ]' * patient_height; % m

%% Reference forces and torques to be applied to the patiend
% Target forces and momentums:
patient_mass = 70; % kg
patient_weight = patient_mass * 9.807 ;
PBWS = 30/100; % Percentage

% Desired forces in N
F_n_x = 100;
F_n_y = 0;
F_n_z = patient_weight*PBWS;

F_n = [F_n_x, F_n_y, F_n_z]';
M_n = [0,0,0]';


%%  Cables Geometry Variables

% Cables attachment positions to the D2W robot frame
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Frame dimentions (cube):
frame_length = 0.89;  %m
frame_height = 1.2;   %m
frame_width  = 0.625; %m
% Fixed Cables Positions variables:
% K_0: D2W frame reference system
% a1, a2 , a3 , a4  : Pelvis Cables attachmented points respect K_0

% D2W frame reference system respect world reference system:
K_0 = [0, 0, 0]'; % Located centered in the button of the D2W frame
a1 = K_0 + [ frame_length/2  frame_width/2 frame_height ]'; % Front left
a2 = K_0 + [ frame_length/2 -frame_width/2 frame_height ]'; % Front right
a3 = K_0 + [-frame_length/2 -frame_width/2 frame_height ]'; % Rare right
a4 = K_0 + [-frame_length/2  frame_width/2 frame_height ]'; % Rare left

% Cables Attachment Positions to End Effector 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Respect pelvis reference system (K_p)
% Fixed Cables Positions variables:
% K_p: Pelvis reference frame
% b1,  b2,  b3,  b4 : Pelvis cables attachmented point to the body

% Pelvis reference system respect K_0:
K_p = K_0 + [0 0 leg_length]';
% Pelvis initial position vectors referenced to the mobile platform
% reference system (K_p)
b1=[ 0.07  pelvis_dimentions(2) 0 ]';
b2=[ 0.07 -pelvis_dimentions(2) 0 ]';
b3=[-0.07 -pelvis_dimentions(2) 0 ]';
b4=[-0.07  pelvis_dimentions(2) 0 ]';
% Reference frames relations
% K_0 and K_p:
% r: Cartesian position vector of the mobile platform respect the world
% reference
% R: rotation of the mobile platform respect the world reference
r = K_p - K_0;
R = [1 0 0 ; 0 1 0 ; 0 0 1];


%% Compute fordward, inverse kinematics and compute cables forces for the desired F_n and M_n:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Fordward kinematics of the pelvis.
% Pelvis desired rotation:
roll  = 0; 
pitch = 0;
yaw   = 0;
R_ref = FUNCS.generalRotationMatrix(roll,pitch,yaw);
% Cables desired lengths:
l_cables = [1, 1, 0.8, 0.8];


% Fordward kinematics for the desired rotation (R_ref) and cables length (l_cables):
%r_ford = FUNCS.fordwardPelvisKinematics(l_cables,R_ref,a1,a2,a3,a4,b1,b2,b3,b4);
 r = [0,0,0.5]';

% Inverse kinematics and visualization for the desired rotation (R_ref) and pelvis position (r_ford):
[l1,l2,l3,l4] = FUNCS.inversePelvisKinematics(r,R_ref,a1,a2,a3,a4,b1,b2,b3,b4);

% Forces caluclation and visualization for the desired rotation (R_ref), pelvis position (r_ford) and F_n and M_n:
%F_r_C = FUNCS.forcesComputation(l1,l2,l3,l4,F_n,M_n,r_ford,a1,a2,a3,a4,b1,b2,b3,b4);
   

